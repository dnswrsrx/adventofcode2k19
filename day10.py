import itertools, math
from collections import OrderedDict


class Asteroid:

    def __init__(self, x, y):
        self.coordinates = (x, y)

    def __repr__(self):
        return str(self.coordinates)

    def tan_angle_and_hypotenuse_to_other_asteroid(self, other):
        angle = None
        adjacent = self.coordinates[0] - other.coordinates[0]
        opposite = self.coordinates[1] - other.coordinates[1]
        if opposite == 0 and adjacent > 0:
            angle = 0
        elif opposite == 0 and adjacent < 0:
            angle = 180
        elif adjacent == 0 and opposite > 0:
            angle = 90
        elif adjacent == 0 and opposite < 0:
            angle = 270

        if angle is None:
            angle = math.degrees(math.atan2(opposite, adjacent))
        if angle < 0:
            angle += 360
        return angle, math.hypot(opposite, adjacent)

    def line_of_sight(self, asteroids):
        return len(self.unique_angles(asteroids))

    def unique_angles(self, asteroids):
        angles = set()
        for asteroid in asteroids:
            if asteroid.coordinates != self.coordinates:
                angles.add(
                    self.tan_angle_and_hypotenuse_to_other_asteroid(
                        asteroid
                    )[0]
                )
        return sorted(list(angles))

    def angles_and_asteroids(self, asteroids):
        output = OrderedDict((angle, [])
            for angle in self.unique_angles(asteroids)
        )
        for asteroid in asteroids:
            if asteroid.coordinates != self.coordinates:
                output[
                    self.tan_angle_and_hypotenuse_to_other_asteroid(asteroid)[0]
                ].append(asteroid)
        for angle in output:
            output[angle].sort(key=lambda asteroid:
                self.tan_angle_and_hypotenuse_to_other_asteroid(asteroid)[1],
                reverse=True
            )
        return output


def collect_asteroids(asteroid_map):
    asteroids = []
    for y_coordinate, row in enumerate(asteroid_map):
        for x_coordinate, coordinates in enumerate(row):
            if coordinates == "#":
                asteroids.append(Asteroid(x_coordinate, y_coordinate))
    return asteroids

def asteroid_with_most_line_of_sight(asteroids):
    return max(
        asteroids,
        key=lambda asteroid: asteroid.line_of_sight(asteroids)
    )

def asteroid_vaporised_at_200_from_station(asteroids):
    station = asteroid_with_most_line_of_sight(asteroids)
    asteroids_by_angles = station.angles_and_asteroids(asteroids)

    unique_angles = station.unique_angles(asteroids)
    index_of_90_degree = unique_angles.index(90)
    angles = unique_angles[index_of_90_degree:] + unique_angles[
        :index_of_90_degree]

    turn = 0
    for angle in itertools.cycle(angles):
        if asteroids_by_angles[angle]:
            asteroid = asteroids_by_angles[angle].pop()
            if turn == 199:
                return 100*asteroid.coordinates[0] + asteroid.coordinates[1]
            turn += 1


if __name__ == "__main__":

    with open("./inputs/day10.txt", "r") as raw_input:
        asteroids = collect_asteroids(raw_input.readlines())

    print(asteroid_with_most_line_of_sight(asteroids).line_of_sight(asteroids))
    print(asteroid_vaporised_at_200_from_station(asteroids))


    # Part 1 Tests
    map = [
        "......#.#.",
        "#..#.#....",
        "..#######.",
        ".#.#.###..",
        ".#..#.....",
        "..#....#.#",
        "#..#....#.",
        ".##.#..###",
        "##...#..#.",
        ".#....####"
    ]
    assert asteroid_with_most_line_of_sight(
        collect_asteroids(map)
    ).line_of_sight(collect_asteroids(map)) == 33

    map = [
        ".#..#",
        ".....",
        "#####",
        "....#",
        "...##",
    ]
    assert asteroid_with_most_line_of_sight(
        collect_asteroids(map)
    ).line_of_sight(collect_asteroids(map)) == 8

    map = [
        ".#..##.###...#######",
        "##.############..##.",
        ".#.######.########.#",
        ".###.#######.####.#.",
        "#####.##.#.##.###.##",
        "..#####..#.#########",
        "####################",
        "#.####....###.#.#.##",
        "##.#################",
        "#####.##.###..####..",
        "..######..##.#######",
        "####.##.####...##..#",
        ".#####..#.######.###",
        "##...#.##########...",
        "#.##########.#######",
        ".####.#.###.###.#.##",
        "....##.##.###..#####",
        ".#.#.###########.###",
        "#.#.#.#####.####.###",
        "###.##.####.##.#..##"
    ]
    assert asteroid_vaporised_at_200_from_station(
        collect_asteroids(map)
    ) == 802

    assert asteroid_with_most_line_of_sight(
        asteroids
    ).line_of_sight(asteroids) == 344

    assert asteroid_vaporised_at_200_from_station(asteroids) == 2732
