from intcode_program import Computer
import itertools


def permutations_of_phase_settings(range_start, range_end):
    return itertools.permutations(range(range_start, range_end), 5)

def signal_for_thrusters(program, phase_setting_permutation, looped=False):
    amplifiers = [
        Computer(program, user_input=[phase_setting], id=id, external_handler=True)
        for id, phase_setting in enumerate(phase_setting_permutation)
    ]
    iterator = itertools.cycle(amplifiers) if looped else amplifiers
    next_input = 0
    for amplifier in iterator:
        if isinstance(next_input, int):
            amplifier.set_input(next_input, to_append=True)
        next_input = amplifier.run()
        if looped:
            if amplifier.id == 4 and amplifier.stop:
                break
    return next_input

def max_thruster_signal(
    program, phase_setting_start, phase_setting_end, looped=False
):
    return max(signal_for_thrusters(program, permutation, looped)
        for permutation in permutations_of_phase_settings(
            phase_setting_start, phase_setting_end
        )
    )

if __name__ == "__main__":
    print(max_thruster_signal("./inputs/day7.txt", 0, 5))
    print(max_thruster_signal("./inputs/day7.txt", 5, 10, True))
