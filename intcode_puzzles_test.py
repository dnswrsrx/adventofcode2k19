from intcode_program import Computer, parse_input

# Day 2
from day2 import brute_force_find_noun_and_verb, program

raw_program = parse_input("./inputs/day2.txt")
com = Computer(program(raw_program), 1)
com.run()
assert com.program[0] == 12490719
assert brute_force_find_noun_and_verb(raw_program, 19690720) == 2003

# Day 5
program = parse_input("./inputs/day5.txt")
com = Computer(program, user_input=1)
assert com.run() == 10987514

com.reset()
com.set_input(5)
assert com.run() == 14195011


program = [3,9,8,9,10,9,4,9,99,-1,8]
com = Computer(program, 1)
assert com.run() == 0
assert com.run() == 0
com = Computer(program, user_input=8)
assert com.run() == 1

program = [3,9,7,9,10,9,4,9,99,-1,8]
com = Computer(program, 1)
assert com.run() == 1
com = Computer(program, 8)
assert com.run() == 0

program = [3,3,1108,-1,8,3,4,3,99]
com = Computer(program, 1)
assert com.run() == 0
com = Computer(program, 8)
assert com.run() == 1

program = [3,3,1107,-1,8,3,4,3,99]
com = Computer(program, 1)
assert com.run() == 1
com = Computer(program, 8)
assert com.run() == 0

program = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]
com = Computer(program, 1)
assert com.run() == 1
com = Computer(program, 0)
assert com.run() == 0

program = [
    3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
]
com = Computer(program, 1)
assert com.run() == 999
com = Computer(program, 8)
assert com.run() == 1000
com = Computer(program, 9)
assert com.run() == 1001

# Day 7

from day7 import max_thruster_signal

assert max_thruster_signal("./inputs/day7.txt", 0, 5) == 20413
assert max_thruster_signal("./inputs/day7.txt", 5, 10, True) == 3321777

program = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]
assert max_thruster_signal(program, 0, 5) == 65210

program = [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]
assert max_thruster_signal(program, 0, 5) == 43210

program = [3,23,3,24,1002,24,10,24,1002,23,-1,23,
101,5,23,23,1,24,23,23,4,23,99,0,0]
assert max_thruster_signal(program, 0, 5) == 54321

program = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5]
assert max_thruster_signal(program, 5, 10, True) == 139629729

program = [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]
assert max_thruster_signal(program, 5, 10, True) == 18216

# Day 9
program = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
computer = Computer(program, debug=True, memory_extension=10000)
# print(computer.run())

program = [1102,34915192,34915192,7,4,7,99,0]
computer = Computer(program)
assert len(str(computer.run())) == 16

program = [104,1125899906842624,99]
computer = Computer(program)
assert computer.run() == program[1]

program = [109, -1, 4, 1, 99]
computer = Computer(program)
assert computer.run() == -1

program = [109, 1, 109, 9, 204, -6, 99]
computer = Computer(program)
assert computer.run() == 204

program = [109, 1, 209, -1, 204, -106, 99]
computer = Computer(program)
assert computer.run() == 204

program = [109,1,203,11,209,8,204,1,99,10,0,42,0]
computer = Computer(program, user_input=667)
assert computer.run() == 667

program = [109, 1, 203, 2, 204, 2, 99]
computer = Computer(program, 5)
assert computer.run() == 5

computer = Computer("./inputs/day9.txt", 1, memory_extension=110)
assert computer.run() == 2518058886
computer.reset()
computer.set_input(2)
assert computer.run() == 44292


# Day 11
from day11 import main
assert len(main("./inputs/day11.txt").keys()) == 1909

# Day 13
from day13 import create_coordinates_representation, play_game
computer = Computer("./inputs/day13.txt", memory_extension=10000, external_handler=True)
assert len(create_coordinates_representation(computer)[2]) == 335
computer.reset()
assert play_game(computer) == 15706
