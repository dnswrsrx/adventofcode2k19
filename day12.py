import copy
import functools
import math
import re

def parse_input(file):
    return (
        list(map(lambda position: int(position), re.findall("-?\d+", line)))
        for line in file
    )


class Planet:

    def __init__(self, position):
        self.x = position[0]
        self.y = position[1]
        self.z = position[2]
        self.x_v = 0
        self.y_v = 0
        self.z_v = 0
        self.other_planets = []

    @staticmethod
    def change_velocity(own, other):
        if own > other:
            return -1
        elif own < other:
            return 1
        return 0

    def apply_gravity(self):
        for other in self.other_planets:
            self.x_v += self.change_velocity(self.x, other.x)
            self.y_v += self.change_velocity(self.y, other.y)
            self.z_v += self.change_velocity(self.z, other.z)

    def apply_velocity(self):
        self.x += self.x_v
        self.y += self.y_v
        self.z += self.z_v

    def __repr__(self):
        return ( f"p: {self.x, self.y, self.z} " +
            f"v: {self.x_v, self.y_v, self.z_v}"
        )

    @property
    def repr(self):
        return (self.x, self.y, self.z, self.x_v, self.y_v, self.z_v)

    @property
    def total_energy(self):
        return (
            sum(map(lambda val: abs(val), (self.x, self.y, self.z))) *
            sum(map(lambda val: abs(val), (self.x_v, self.y_v, self.z_v)))
        )


def assign_other_planets(planets):
    for planet in planets:
        for tentative_other in planets:
            if planet != tentative_other:
                planet.other_planets.append(tentative_other)
    return planets

def simulate(planets, number_of_iteration=1):
    planets = copy.deepcopy(planets)
    for step in range(number_of_iteration):
        for planet in planets:
            planet.apply_gravity()
        for planet in planets:
            planet.apply_velocity()
    return planets


# Part 2 stuff
def lcmm(*args):
    return functools.reduce(lcm, args)

def lcm(a, b):
    return abs(a*b) // math.gcd(a, b)

def simulate_until_history_repeated(planets):
    planets = copy.deepcopy(planets)
    history_x = set()
    history_y = set()
    history_z = set()
    step = 0
    step_x = 0
    step_y = 0
    step_z = 0
    while True:
        if step_x == 0:
            state_x = tuple((planet.x, planet.x_v) for planet in planets)
            if state_x in history_x:
                step_x = step
            else:
                history_x.add(state_x)

        if step_y == 0:
            state_y = tuple((planet.y, planet.y_v) for planet in planets)
            if state_y in history_y:
                step_y = step
            else:
                history_y.add(state_y)

        if step_z == 0:
            state_z = tuple((planet.z, planet.z_v) for planet in planets)
            if state_z in history_z:
                step_z = step
            else:
                history_z.add(state_z)

        if 0 not in [step_x, step_y, step_z]:
            return lcmm(step_x, step_y, step_z)

        for planet in planets:
            planet.apply_gravity()
        for planet in planets:
            planet.apply_velocity()

        step += 1



if __name__ == "__main__":
    with open("./inputs/day12.txt", "r") as file:
        planets = [Planet(line) for line in parse_input(file)]
        planets = assign_other_planets(planets)
    print(sum(planet.total_energy for planet in simulate(planets, 1000)))
    print(simulate_until_history_repeated(planets))

    assert sum(planet.total_energy for planet in simulate(planets, 1000)) == 7013
    assert simulate_until_history_repeated(planets) == 324618307124784
