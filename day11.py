import itertools
from intcode_program import Computer

DIRECTION_DELTA = [(-1, 0), (0, -1), (1, 0), (0, 1)]

def main(program, custom_colour=0):

    coordinates_and_colour = {(0, 0): custom_colour}
    last_coordinates = (0, 0)
    last_facing_direction = 1

    computer = Computer(program, external_handler=True, memory_extension=500)

    while not computer.stop:
        computer.set_input(coordinates_and_colour.get(last_coordinates, 0))
        coordinates_and_colour[last_coordinates] = computer.run()
        output = computer.run()
        last_facing_direction += output if output else -1
        last_coordinates = (
            last_coordinates[0]+DIRECTION_DELTA[last_facing_direction % 4][0],
            last_coordinates[1]+DIRECTION_DELTA[last_facing_direction % 4][1]
        )
    return coordinates_and_colour

def registration_identifier(coordinates_and_colour):
    x_transform = abs(
        min(coordinates[0] for coordinates in coordinates_and_colour.keys())
    )
    y_transform = abs(
        min(coordinates[1] for coordinates in coordinates_and_colour.keys())
    )
    max_x = max(coordinates[0] for coordinates in coordinates_and_colour.keys())
    max_y = max(coordinates[1] for coordinates in coordinates_and_colour.keys())

    for y in range(max_y + y_transform + 1):
        for x in range(max_x + x_transform + 1):
            coordinates = (x-x_transform, y-y_transform)
            to_print = ("#" if coordinates in coordinates_and_colour and
            coordinates_and_colour[coordinates] else " ")
            print(to_print, end=" ")
        print()


if __name__ == "__main__":
    print(len(main("./inputs/day11.txt").keys()))
    print(registration_identifier(main("./inputs/day11.txt", custom_colour=1)))
