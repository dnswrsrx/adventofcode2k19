from intcode_program import Computer


if __name__ == "__main__":
    computer = Computer("./inputs/day9.txt", memory_extension=110)
    computer.set_input(1)
    print(computer.run())
    computer.reset()
    computer.set_input(2)
    print(computer.run())
