def parse_input(file):
    return (int(mass.strip()) for mass in file.readlines())

def calculate_fuel(mass):
    return (mass // 3) - 2

def collect_fuel_requirements(masses):
    return [calculate_fuel(mass) for mass in masses]

def calculate_additional_fuel(fuel):
    if (additional_fuel := calculate_fuel(fuel)) <= 0:
        return fuel
    return fuel + calculate_additional_fuel(additional_fuel)

def collect_additional_fuel_requirements(fuel_requirements):
    return (calculate_additional_fuel(fuel_requirement)
        for fuel_requirement in fuel_requirements
    )


if __name__ == "__main__":
    with open("./inputs/day1.txt", "r") as file:
        masses = parse_input(file)

    fuel_requirements = collect_fuel_requirements(masses)
    print(sum(fuel_requirements))
    print(sum(collect_additional_fuel_requirements(fuel_requirements)))

    # Answer checking
    # assert sum(fuel_requirements) == 3369286
    # assert sum(collect_additional_fuel_requirements(
    #     fuel_requirements
    # )) == 5051054
