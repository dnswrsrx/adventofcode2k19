require Computer

defmodule Day5 do
  @memory Computer.parse("inputs/day5.txt")

  defp run_task(input) do
    task = Task.async(fn ->
      send(self(), input)
      Computer.run(@memory)
    end)
    Map.get(Task.await(task), :output)
  end

  def part1(), do: run_task(1)
  def part2(), do: run_task(5)
end
