defmodule Computer do

  def parse(memory) when is_bitstring(memory) do
    parse(
      String.split(File.read!(memory), ",")
      |> Stream.map(&(String.trim(&1) |> String.to_integer()))
      |> Stream.with_index
      |> Stream.map(fn {code, index} -> {index, code} end)
      |> Map.new
    )
  end

  def parse(memory) when is_list(memory) do
    parse(
      memory
      |> Stream.with_index
      |> Stream.map(fn {code, index} -> {index, code} end)
      |> Map.new
    )
  end

  def parse(memory) when is_map(memory), do: memory

  def run({ memory, index }), do: run(memory, index)
  def run(memory, index \\ 0) do
    case Map.get(memory, index) do
      99 -> memory
      _ -> run(execute(memory, index))
    end
  end

  defp execute(memory, index) do
    {opcode, modes} = parse_opcode(Map.get(memory, index))
    case opcode do
      1 -> operate(&+/2, memory, index, modes)
      2 -> operate(&*/2, memory, index, modes)
      3 -> storage(memory, index)
      4 -> storage(memory, index, modes)
      5 -> jump(&!==/2, memory, index, modes)
      6 -> jump(&===/2, memory, index, modes)
      7 -> operate(&(&1 < &2 && 1 || 0), memory, index, modes)
      8 -> operate(&(&1 === &2 && 1 || 0), memory, index, modes)
    end
  end

  defp parse_opcode(opcode) do
    opcode = Integer.to_string(opcode)
    padded_opcode = String.pad_leading(opcode, pad_with(opcode), "0")
    {modes, opcode} = String.split_at(padded_opcode, -2)
    { String.to_integer(opcode), modes |> String.reverse() |> String.split("", trim: true) |> Enum.map(&String.to_integer/1) }
  end

  defp pad_with(opcode) do
    cond do
      String.ends_with?(opcode, ["1", "2", "7", "8"]) -> 5
      String.ends_with?(opcode, "4") -> 3
      String.ends_with?(opcode, ["5", "6"]) -> 4
      true -> 0
    end
  end

  defp operate(operator, memory, index, modes) do
    {
      Map.put(
        memory,
        Map.get(memory, index+3),
        operator.(
          value(memory, Map.get(memory, index+1), Enum.at(modes, 0)),
          value(memory, Map.get(memory, index+2), Enum.at(modes, 1))
        )
      ),
      index+4
    }
  end

  defp storage(memory, index) do
    # IO.inspect({Map.get(memory, :id), "here"})
    receive do
      input -> { Map.put(memory, value(memory, index+1), input), index+2}
    end
  end

  defp storage(memory, index, modes) do
    output = Map.get(memory, value(memory, index+1, List.first(modes)))
    if Map.get(memory, :ext) && is_atom(Map.get(memory, :ext)) do
      try do
        send(Map.get(memory, :ext), output)
      rescue
        _ -> :e
      end
    end
    { Map.put(memory, :output, output), index+2 }
  end

  defp jump(equality, memory, index, modes) do
    { memory,
      equality.(0, Map.get(memory, value(memory, index+1, List.first(modes))))
      && Map.get(memory, value(memory, index+2, List.last(modes))) || index+3
    }
  end

  defp value(memory, value_or_index, mode \\ 0) do
    case mode do
      0 -> Map.get(memory, value_or_index)
      1 -> value_or_index
    end
  end
end
