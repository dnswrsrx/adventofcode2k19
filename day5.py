from intcode_program import Computer

computer = Computer("./inputs/day5.txt")
computer.set_input(1)
print(computer.run())

computer.reset()
computer.set_input(5)
print(computer.run())
