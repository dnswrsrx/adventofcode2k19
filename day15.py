import copy
import os

from intcode_program import Computer


def computer(program):
    com = Computer(program, user_input=[], external_handler=True)
    return com

def print_map(map, robot=None):

    min_x = min(coord[0] for coord in map)
    min_y = min(coord[1] for coord in map)
    max_x = max(coord[0] for coord in map) + 1
    max_y = max(coord[1] for coord in map) + 1

    for y in range(min_y, max_y):
        for x in range(min_x, max_x):
            coord = (x, y)
            if coord == (0, 0):
                print("_", end=" ")

            item = map.get(coord)
            if item == "wall":
                print("w", end=" ")
            elif item == "oxygen":
                print("o", end=" ")
            elif robot and coord == robot:
                print("r", end=" ")
            elif item == "path":
                print(" ", end=" ")
            else:
                print("?", end=" ")
        print("")
    os.system("clear")


DIRECTION = {1: (0, 1), 2: (0, -1), 3: (-1, 0), 4: (1, 0)}


class Coordinates:

    def __init__(self, coordinates):
        self.coordinates = coordinates

    def neighbouring_coordinates(self, direction):
        delta = DIRECTION.get(direction)
        return (self.coordinates[0] + delta[0], self.coordinates[1] + delta[1])


class MapCoordinates(Coordinates):

    def __init__(
        self,
        coordinates,
        program,
        steps_from_origin=0,
        direction_from_parent=None,
        origin=False
    ):
        super().__init__(coordinates)
        self.program = program
        self.steps_from_origin = steps_from_origin
        self.direction_from_parent = direction_from_parent

        self.changed_program = None
        self.kind = None

        if origin:
            self.kind = "path"
            self.changed_program = program

    def neighbours(self):
        for direction in DIRECTION:
            com = computer(self.program)
            neighbour = MapCoordinates(
                self.neighbouring_coordinates(direction),
                self.changed_program,
                self.steps_from_origin+1,
                direction
            )
            neighbour.get_kind()
            yield neighbour

    def get_kind(self):
        com = computer(self.program)
        com.set_input(self.direction_from_parent)
        self.kind = ["wall", "path", "oxygen"][com.run()]
        self.changed_program = com.program
        return self.kind


class Robot:

    def __init__(self, program):
        self.program = program
        self.steps_to_oxygen = 0
        self.oxygen_location = None
        self.map = {(0, 0): "path"}

    def make_map(self):
        origin = MapCoordinates((0, 0), self.program, origin=True)
        self.explore(origin)
        return self.map

    def explore(self, origin):
        for neighbour in origin.neighbours():

            print_map(self.map, robot=origin.coordinates)

            if neighbour.coordinates not in self.map:
                self.map[neighbour.coordinates] = neighbour.kind
                if neighbour.kind != "wall":
                    if neighbour.kind == "oxygen":
                        self.oxygen_location = neighbour.coordinates
                        self.steps_to_oxygen = neighbour.steps_from_origin
                    self.explore(neighbour)


class Oxygen(Coordinates):

    def __init__(self, coordinates):
        super().__init__(coordinates)
        self.deoxygenated_neighbours = []

    def get_deoxygenated_neighbours(self, map):
        for direction in DIRECTION:
            if map.get(coordinates := self.neighbouring_coordinates(direction)) == "path":
                self.deoxygenated_neighbours.append(coordinates)
        return self.deoxygenated_neighbours


class Map:

    def __init__(self, map, oxygen_source):
        self.map = copy.deepcopy(map)
        self.leaves_stack = [[oxygen_source]]
        self.iteration = 0

    def fill_oxygen(self):

        while self.leaves_stack:
            print_map(self.map)
            new_stack = []
            current = self.leaves_stack.pop(0)
            for leaf in current:
                for coordinate in leaf.get_deoxygenated_neighbours(self.map):
                    new_stack.append(Oxygen(coordinate))
                    self.map[coordinate] = "oxygen"
            if new_stack:
                self.iteration += 1
                self.leaves_stack.append(new_stack)
        return self.iteration


if __name__ == "__main__":
    robot = Robot("inputs/day15.txt")
    complete_map = robot.make_map()

    map = Map(complete_map, Oxygen(robot.oxygen_location))
    assert robot.steps_to_oxygen == 280
    assert map.fill_oxygen() == 400
