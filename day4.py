import more_itertools

def parse_input(file):
    return [int(value) for value in file.readline().strip().split("-")]

def count_valid_passwords(password_range):
    part_one_count = 0
    part_two_count = 0
    for password in filter(
        always_increasing,
        range(password_range[0], password_range[1])
    ):
        matching_numbers = adjacent_numbers(password)
        if len(matching_numbers) != 0:
            part_one_count += 1
        if additional_condition(matching_numbers, password):
            part_two_count += 1
    return part_one_count, part_two_count

def always_increasing(password):
    for pair in more_itertools.pairwise(str(password)):
        if int(pair[0]) > int(pair[1]):
            return False
    return True

def adjacent_numbers(password):
    return set(pair[0]
        for pair in more_itertools.pairwise(str(password))
        if pair[0] == pair[1]
    )

def additional_condition(matching_numbers, password):
    for number in matching_numbers:
        if str(password).count(str(number)) == 2:
            return True
    return False


if __name__ == "__main__":
    with open("./inputs/day4.txt", "r") as file:
        password_range = parse_input(file)

    print(count_valid_passwords(password_range))

    # Answer checking
    assert count_valid_passwords(password_range) == (1653, 1133)
