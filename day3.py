from collections import Counter, namedtuple

def parse_input(file):
    return [movements.strip().split(",") for movements in file.readlines()]

Point = namedtuple("Point", ["x", "y"])

def collect_points_traversed_by_wire(path):
    traversed = [Point(0, 0)]
    for movement in path:
        traversed.extend(points_traversed(
            traversed[-1],
            movement
            )
        )
    return traversed

def points_traversed(start, movement):
    distance = int(movement[1:])
    if "R" in movement:
        return collect_points_traversed_by_movement(
            start, 1, "x", distance
        )
    elif "U" in movement:
        return collect_points_traversed_by_movement(
            start, 1, "y", distance
        )
    elif "L" in movement:
        return collect_points_traversed_by_movement(
            start, -1, "x", distance
        )
    elif "D" in movement:
        return collect_points_traversed_by_movement(
            start, -1, "y", distance
        )

def collect_points_traversed_by_movement(
    start,
    direction,
    axis,
    distance
):
    if axis == "x":
        return (Point(start.x+(delta*direction), start.y)
            for delta in range(1, distance+1)
        )
    return (Point(start.x, start.y+(delta*direction))
        for delta in range(1, distance+1)
    )

def get_crossing_points(paths):
    points_traversed_by_wire_1 = set(collect_points_traversed_by_wire(
        paths[0]
    ))
    points_traversed_by_wire_2 = set(collect_points_traversed_by_wire(
        paths[1]
    ))
    return (point for point in points_traversed_by_wire_2
        if point in points_traversed_by_wire_1
        and point != Point(0, 0)
    )

def get_crossing_point_closest_to_origin(paths):
    return min(manhattan_distance_to_origin(point) for point in
        get_crossing_points(paths)
    )

def manhattan_distance_to_origin(point):
    return abs(0 - point.x) + abs(0 - point.y)

def get_minimum_steps_taken_to_crossing_points(paths):
    points_travelled_by_1 = collect_points_traversed_by_wire(
        paths[0]
    )
    points_travelled_by_2 = collect_points_traversed_by_wire(
        paths[1]
    )

    return min(
        points_travelled_by_1.index(point) +
        points_travelled_by_2.index(point)
        for point in get_crossing_points(paths)
    )


if __name__ == "__main__":
    with open("./inputs/day3.txt", "r") as file:
        paths = parse_input(file)

    print(get_crossing_point_closest_to_origin(paths))
    print(get_minimum_steps_taken_to_crossing_points(paths))

    # Answer checking
    assert get_crossing_point_closest_to_origin(paths) == 446
    assert get_minimum_steps_taken_to_crossing_points(paths) == 9006
